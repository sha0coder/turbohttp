import std.stdio;

enum paint:string {
	clean = "\033[0m", // clear the color
	clear = "\033[2K", // clear the line
	bold = "\033[1m",
	italic = "\033[3m",
	underline = "\033[4m",
	inverse = "\033[7m",

	white = "\033[37m",
	grey = "\033[90m",
	black = "\033[30m",

	blue = "\033[34m",
	cyan = "\033[36m",
	green = "\033[32m",
	magenta = "\033[35m",
	red = "\033[31m",
	yellow = "\033[33m"
};





/*

color = {
	'clean'	  : '\033[0m',  # Clear color
	'clear'	  : '\033[2K',  # Clear line
	'bold'      : ['\033[1m',  '\033[22m'],
	'italic'    : ['',  '\033[23m'],
	'underline' : ['',  '\033[24m'],
	'inverse'   : ['',  '\033[27m'],

	#grayscale
	'white'     : ['', '\033[39m'],
	'grey'      : ['', '\033[39m'],
	'black'     : ['', '\033[39m'],
	
	#colors
	'blue'      : ['', '\033[39m'],
	'cyan'      : ['', '\033[39m'],
	'green'     : ['', '\033[39m'],
	'magenta'   : ['', '\033[39m'],
	'red'       : ['', '\033[39m'],
	'yellow'    : ['', '\033[39m']
}

*/