/*
	Very fast, epoll based HTTP bruteforcing library

	author: @sha0coder

*/

import std.string;
import std.socket;
import std.array;
import std.stdio;
import std.conv;

import turbosockets;
import lifo;








class TurboHTTP : TurboSockets {
	public bool isReady;
	
	protected string host;
	protected string ip;
	protected ushort port;
	protected string item[int];
	protected Lifo!string queue;
	
	private string httpHost;
	private string method;
	private const string httpVersion = " HTTP/1.1\n";
	private const string headers = "User-agent: Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0\nConnection: Keep-Alive\n\n";
	

	
	public this(string host, ushort port, int pool, string method) {
		this.isReady = false;
		this.host = host;
		this.port = port;
		this.httpHost = "Host: "~host~"\n";
		this.method = method~" ";
		this.ip = resolveDNS(host);
		super(ip, port, pool); // TurboSockets
		
		if (ip.length>0)
			isReady = true;
		
	}
	
	public void addQueue(Lifo!string queue) {
		this.queue = queue;
	}
	
	public string resolveDNS(string host) {
		string ip;
		
		auto ih = new InternetHost();
		if (ih.getHostByName(host)) {
			return InternetAddress.addrToString(ih.addrList[0]);
		} else {
			writeln("cant resolve "~host~"!!");
			return "";
		}
	}
	
	override protected char *onWrite(int sfd) {
		if (queue.empty()) {
			writeln("queue empty!!!!");
			this.stop();
			return null;
		}
		
		item[sfd] = queue.pop();
		string req = method ~ item[sfd] ~ httpVersion ~ httpHost ~ headers ~ "\x00";

		return cast(char*)req; 
	}
	
}
