import std.datetime;
import std.stdio;

class Crono {
	
	private bool isRunning;
	private long tstart;
	private long tstop;
	
	public this() {
		tstart = tstop = 0;
		isRunning = false;
	}
	
	public void start() {
		this.tstart = Clock.currStdTime();
		isRunning = true;
	}
	
	public void stop() {
		this.tstop = Clock.currStdTime();
		isRunning = false;
	}
	
	public long getCrono()  {
		long duration = 0;
		
		if (isRunning)
			duration = Clock.currStdTime()-tstart;
		else
			duration = tstop-tstart;
		
		return duration;
	}
}


