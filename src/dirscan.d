/*
	dirscan, very fast web scanner and crawler, for discovering directories and objects.

	author: @sha0coder

	- hight speed epoll loop  
	- cpu friendly
	- overridable async events
	- http keep alive -> less reconnection ratio
	- http head -> less bytes to parse
	- no clones/forks/threads of any types
	



		TurboSockets
			|
			V
        TurboHTTP  ----- Lifo Queue
        	|
        	V
         Dirscan ------ Wordlist object


*/

import core.exception;
import std.algorithm;
import std.stdio;
import std.array;
import std.conv;

import turbohttp;
import httparser;
import wordlist;
import crono;
import lifo;


class DirScan : TurboHTTP {
	private Wordlist wl;
	private string[] extensions;	
	private HTTParser httparser;
	private ulong sent;
	private Crono crono;
	
	public this(string host, ushort port, int pool) {
		super(host,port,pool,"HEAD");
		httparser = new HTTParser(host,ip,port);
		wl = new Wordlist();
		sent = 0;
		crono = new Crono();
	}
	
	public void load(string filename, string path) {
		wl.load(filename);
		queueDir(path);
		crono.start();
	}
	
	public void setExtensions(string[] extensions) {
		this.extensions = extensions;
	}
	
	private void queueDir(string dir2) {
		string dir = dir2.dup;
		if (dir.length==0)
			return;
			
		foreach(word; wl.get()) {
			foreach(ext; extensions) {
				queue.push(dir~word~ext);
			}
			queue.push(dir~word~"/");
		}
	}
	
	override protected void onRead(int sfd, size_t sz, string buff) {
		string resp = to!string(buff);
		this.sent++;
		
		
		try {
			//stdout.writefln("%d wps %s",(this.sent/this.crono.getCrono()),item[sfd]);
			//stdout.flush();
			
			httparser.parse(resp, item[sfd]);
				
				
			switch (httparser.getRespType()) {
				
				case http_resp.FOUND_DIRNOLIST:
					queueDir(item[sfd].dup);
					break;
					
				case http_resp.FOUND_DIRINDEX: 
					queueDir(item[sfd].dup);
					// do crawl
					break;
					
				case http_resp.REDIRECT:
					queueDir(httparser.getRedirection());
					break;
					// TODO: do this in a new task? export to function
		
				default:
					break;
			}
			
		} catch (RangeError e) {
			writefln("range error socket: %d ",sfd);
		}
		
		item[sfd] = "";
	}
	
	override protected void onSiteDown() {
		writeln("The site is down, reduce the pool!!");
	}
	
	override protected void onFinish() {
		writeln("finish");
	}
	
}





void usage(string procname) {
	writeln("USAGE: "~procname~" [wordlist] [extensions] [host:port or ip:port] [start path] [connection pool]");
	writeln("example: "~procname~" words/words.txt '.php,.html' labs.badchecksum.net:80 / 20\n");
}

int main(string[] args) {
	string[] extensions;
	string file;
	string host;
	string path;
	string ip;
	ushort port;
	int pool;
	
	if (args.length != 6) {
		usage(args[0]);
		return -1;
	}
	
	try {
		file = args[1];
		extensions = split(args[2],",");
		pool = to!int(args[5]);
		path = args[4];
		
		string[] hostport = split(args[3],":");
		host = hostport[0];
		port = to!ushort(hostport[1]);
	
	} catch (Exception e) {
		writeln(e);
		usage(args[0]);
		return -1;
	}

	
	auto dirscan = new DirScan(host,port,pool);
	if (dirscan.isReady) {
		dirscan.load(file,path);
		dirscan.start();
	}
	
	return 0;
}