/*
	TurboSockets epoll event loop, by @sha0coder 

	Future version will use this native d epoll loop
	the current software is using socketpool.c

	TODO: 
		- provide massive socket close method and/or destructor
		- user errno to check the connection EINPROGRESS		
*/

import core.sys.posix.sys.socket;
import core.sys.posix.netinet.in_;
import core.sys.posix.fcntl;
import core.stdc.string;
import std.c.stdlib;
import std.stdio;
import std.conv;
import epoll;	

extern (C) int close(int fd);
enum SO_REUSEPORT = 15;


class TurboSockets {
	private int efd;
	private bool isRunning;
	private sockaddr_in target;
	private void delegate(int sfd, size_t sz, string buff) cbRead;
	private string delegate(int sfd) cbWrite;
	private int pool;
	protected int TIMEOUT = 1000;
	protected int BUFFER  = 1024*5;
	protected bool reconnect;
	
	public this(string ip, ushort port, int pool) {
		//setInterrupt();
		
		this.pool = pool;
		target = getSockaddr(ip,port);
		reconnect = true;
	}
	
	public void onRead(void delegate(int sfd, size_t sz, string buff) cbRead) {
		this.cbRead = cbRead;
	}
	
	public void onWrite(string delegate(int sfd) cbWrite) {
		this.cbWrite = cbWrite; 
	}
	
	public void start() {
		static epoll_event *events;
		char *buff;
		int c;
		isRunning = true;
		
		efd = epoll_create(pool);
		
		foreach(int i; 0..pool)
			newConnection();
	
	
		if ((buff = cast(char *)malloc(BUFFER+4)) == null) {
			throw new Exception("no memory :/");
			return;
		}
	
		if ((events = cast(epoll_event *)calloc(pool, epoll_event.sizeof)) == null) {
			throw new Exception("no memory :/");
			return;
		}
	
		do {
			c = epoll_wait(efd, events, pool, TIMEOUT);
			if (c == -1) {
				writeln("epoll_wait problem");
				continue;
				//return;
			}
			
			if (c == 0) 
				onTimeout();
	
			foreach(i; 0..c) {
	
				if (events[i].events & (EPOLL_EVENTS.EPOLLRDHUP | EPOLL_EVENTS.EPOLLHUP))
					evDoReconnect(&events[i]);
	
						
				else {
					if (events[i].events & EPOLL_EVENTS.EPOLLOUT)
						evDoWrite(&events[i]);

					if (events[i].events & EPOLL_EVENTS.EPOLLIN)				// pirorize receiving before sending
						evDoRead(&events[i], buff);
	
				}
	
			}
	
		} while(isRunning);
	
		onFinish();
	}
	
	public void stop() {
		isRunning = false;
	}
	
	private int getSocket() {
		int sfd;
		int yes = 1;
	
		if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			throw new Exception("cant create sockets");
		}
	
		if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, int.sizeof) <0) {
			throw new Exception("cant set socket options");
		}
		
		//setsockopt(sfd, SOL_SOCKET, SO_REUSEPORT, &yes, int.sizeof);
	
		if (fcntl(sfd, F_SETFL, O_NONBLOCK) == -1) {
			throw new Exception("cant set nonblocking socket");
		}
	
		return sfd;
	}
	
	private sockaddr_in getSockaddr(string ip, ushort port) {
	   static sockaddr_in sa;
	
	   ip ~= "\x00";
	   memset(&sa, 0, sockaddr_in.sizeof);
	
	   sa.sin_family = AF_INET;
	   sa.sin_addr.s_addr = inet_addr(ip.dup.ptr);
	   sa.sin_port = htons(port);
	   
	   return sa;
	}
	
	private void doConnect(int sfd) {
		connect(sfd, cast(sockaddr *)&target, sockaddr.sizeof);
		/*
			close(sfd);
			throw new Exception("can't connect try again"); //TODO: reintentar?
		}*/
	}
	
	private int isSocketError(int sfd) {
	   int ret;
	   int code;
	   size_t len = int.sizeof;
	
	   ret = getsockopt(sfd, SOL_SOCKET, SO_ERROR, cast(void *)&code, cast(uint *)&len);
	   if ((ret || code)!= 0)
	      return 1;
	
	   return 0;
	}
	
	private void addEvent(int sfd) {
		static epoll_event ev;
	
		ev.events = EPOLL_EVENTS.EPOLLOUT | EPOLL_EVENTS.EPOLLIN | EPOLL_EVENTS.EPOLLRDHUP | EPOLL_EVENTS.EPOLLERR | EPOLL_EVENTS.EPOLLET;
		ev.data.fd = sfd;
	
		if (epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &ev) != 0) {
			throw new Exception("epoll_ctl, adding event\n");
		}
	}
	
	private void delEvent(int sfd) {
		epoll_ctl(efd, EPOLL_CTL_DEL, sfd, null);
	}
	
		
	private void evDoWrite(epoll_event *ev) {
		string data;
		size_t bytes;
		ulong sz;
		char *ptr;
	
		if (isSocketError(ev.data.fd) != 0) {
			//perror("write isSocketError SITE DOWN!");
			evDoReconnect(ev);
			return;
		}
	
		data = cbWrite(ev.data.fd).dup;
		if (data.length == 0)
			return;
		
		data ~= "\x00";			
		
		if ((bytes = send(ev.data.fd, cast(char *)data, cast(int)(data.length-1), 0)) < 0) {
			throw new Exception("send failed");
			return;
		}
	
		if (bytes < sz) {
			printf("sent %d bytes from a %d buffer\n",bytes,sz);
		}
	
		//printf("sending %d bytes!!\n",bytes);
	
	}
	
	private void evDoRead(epoll_event *ev, char *buff) {
		size_t bytes;
		
		writeln("evread");
	
		if (isSocketError(ev.data.fd) != 0) {
			//perror("read isSocketError SITE DOWN!");
			
			evDoReconnect(ev);
			//stop();
			//onSiteDown();
			return;
		}
	
	    memset(buff,0x0,BUFFER+2);
	    if ((bytes = recv(ev.data.fd, buff, BUFFER, 0)) < 0) {
			throw new Exception("recv failed");
			return;
		}
	    
	   	//printf("recv %d bytes!!\n",bytes);
	
	    if (bytes>0)  // listo para leer y leo cero, ?? reconectar?
	    	cbRead(ev.data.fd,bytes,to!string(buff));
	}
	
	private void evDoReconnect(epoll_event *ev) {
		//writeln("reconnecting");
		delEvent(ev.data.fd);
		if (close(ev.data.fd)!=0) { //todo shutdown
			throw new Exception("reconnection - can't close :/");
			return;
		}
		
		if (!reconnect) {
			stop();
			return;
		}
			
		//printf("reconnecting\n");
		newConnection();
	
		if (ev.events & EPOLL_EVENTS.EPOLLERR) {
			//writeln("socket error");	
			return;
		}
	}
	
	private void newConnection() {
		int sfd = getSocket();
		doConnect(sfd);
		addEvent(sfd);
	}
	
	
	protected void onSiteDown() {}
	protected void onFinish() {}
	protected void onTimeout() {}
	
}

/*
void main()	 {
	writeln("testing ..");
	bool done = false;
	auto ts = new TurboSockets("91.121.172.164",80,1);
	ts.onRead((int sfd, size_t sz, string resp) {
		writeln("evento read");
		writeln(resp); 
		ts.stop();
	});
	ts.onWrite((int sfd) {
			//if (!done) {
				writeln("evento write");
				done = true;
				return "GET / \n\n";
			//} else 
				//return "";
	});		
	ts.start();
}*/


