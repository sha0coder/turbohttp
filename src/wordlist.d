import std.stdio;
import std.file;
import std.conv;


class Wordlist {
	private string[] words;
	
	public void load(string filename) {
		File fd = File(filename);      	// for big wordlists use mmap
		foreach (line; fd.byLine) {
			string w = cast(string)line;
			words ~= w.idup; // .idup?
		}
		fd.close();
		writeln(to!string(words.length)~" words loaded.");
	}
	
	public size_t size() {
		return words.length;
	}
	
	public string[] get() {
		return words;
	}
}