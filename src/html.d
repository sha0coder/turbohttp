import std.stdio;
import std.regex;
import std.string;
import url;

class HTML {

	private URL url;
	private Regex!(char) href;
	private Regex!(char) indexof;
	private string html;
	private string[] urls;

	public this(string html, URL url) {
		this.url = url;
		this.html = html;
		href = regex("href *= *[\"']([^\"']+)[\"']");   // TODO: usar ctRegex()
	}

	public bool isDirlisting() {
		return (indexOf(html, "<title>Index of /") >= 0);
	}

	public string[] getUrls() {
		foreach (m; match(html, href)) {

			if (startsWith(m[1],"?") || startsWith(m[1],"mailto:") || m[1] == "/")
				continue;

			if (startsWith(m[1],"http")) {
				URL u = new URL(m[1]);
				if (u.getHost() == url.getHost()) {
					urls ~= m[1];
				}

			} else if (startsWith(m[1],"/")) {
				urls ~= url.getBase() ~ m[1];

			} else {
				urls ~= url.getBase() ~ url.getDirectory() ~ m[1];

			}
			
		}

		return null;
	}

	
}

