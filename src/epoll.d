/*
	the epoll.h definitions for d
	from: https://github.com/adamdruppe/misc-stuff-including-D-programming-language-web-stuff/blob/master/eventloop.d#L697
*/

module epoll;

version(linux) {
        extern(C):

        alias int c_int; 

        alias uint uint32_t;
        alias ulong uint64_t;

        union epoll_data {
                void    *ptr;
                int      fd;
                uint32_t u32;
                uint64_t u64;
        }

        struct epoll_event {
                uint32_t   events;    /* Epoll events */
                epoll_data data;      /* User data variable */
        }

        enum EPOLL_CTL_ADD = 1;
        enum EPOLL_CTL_DEL = 2;
        enum EPOLL_CTL_MOD = 3;


        import std.conv : octal;
        enum {
                EPOLL_CLOEXEC = octal!"2000000",
                EPOLL_NONBLOCK = octal!"4000"
        }

        enum EPOLL_EVENTS {
                EPOLLIN = 0x001,
                EPOLLPRI = 0x002,
                EPOLLOUT = 0x004,
                EPOLLRDNORM = 0x040,
                EPOLLRDBAND = 0x080,
                EPOLLWRNORM = 0x100,
                EPOLLWRBAND = 0x200,
                EPOLLMSG = 0x400,
                EPOLLERR = 0x008,
                EPOLLHUP = 0x010,
                EPOLLRDHUP = 0x2000,
                EPOLLONESHOT = (1 << 30),
                EPOLLET = (1 << 31)
        }

        int epoll_create(int sz);
        int epoll_create1(int flags);
        int epoll_ctl(int epfd, int op, int fd, epoll_event* event);
        int epoll_wait(int epfd, epoll_event* events, int maxevents, int timeout);

        //import core.sys.posix.sys.time;
}
