/*
	
	The class object used to scan is TurboHTTP, 
	this is a simple async http class is only used to download html content.
	
	by @sha0coder
*/

module http;

import std.stdio;
import turbosockets;

class HTTP : TurboSockets {
	private const string httpVersion = " HTTP/1.1\n";
	private const string headers = "\nUser-agent: Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0\nConnection: Keep-Alive\n\n";
	private string html;
	private string request;
	private string host;
	
	
	public this(string host, string ip, ushort port) {
		this.host = host;	
		super(ip,port,1);
		TIMEOUT = 1000;
		reconnect = false;
	}
	
	public void download(string path) {	
		html = "";
		buildRequest("GET",path);
		this.start();
	}
	
	public void head(string path) {
		html = "";
		buildRequest("HEAD",path);
		this.start();
	}
	
	public string read() {
		return html.dup;
	}
 	
	
	
	private void buildRequest(string method, string path) {
		this.request = method ~ " " ~ path ~ httpVersion ~ "Host: " ~ host ~ headers ~ "\x00";
	}
	
	override protected void onRead(int sfd, size_t sz, string buff) {
		html ~= buff;
		request = null;
	}
	
	override protected char *onWrite(int sfd) {
		return cast(char*)request;
	}
	
	override protected void onSiteDown() {
		writeln("site down ://");
	}
	
	/*
	override protected void onFinish() {
		//callback(html);
	}*/
	
	override protected void onTimeout() {
		stop();
	}
	
}







unittest {
	auto http = new HTTP("labs.badchecksum.net","91.121.85.145",80);
	http.download("/index.php",(string html) {writefln("uno: %d",html.length);});
	http.download("/index.php",(string html) {writefln("dos: %d",html.length);});
}