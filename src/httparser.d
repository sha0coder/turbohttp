/*
	HTTP response classificator
	@sha0coder
*/

module httparser;


import std.algorithm;
import std.string;
import std.stdio;
import std.conv;
import color;
import html;
import http;
import url;



enum http_resp {
	FOUND_DIRLIST,		// directory listing should be CRAWLED
	FOUND_DIRNOLIST,	// there is a directory but not listed, should be BRUTEFORCED
	FOUND_DIRINDEX,		// there is a directory not listed, but there is an index.php should be CRAWLED the content and BRUTEFORCED 
	FOUND_OBJ,			// there is an object, should be CRAWLED
	REDIRECT,			// there is a redirection, should be BRUTEFORCED 
	AUTH,				// do nothing by now
	NOT_FOUND,			// do nothing
	FAIL,				// do nothing
	OTHER,				// do noghing
}

class HTTParser {
	private string hdrs;
	private string path;
	private string host;
	private string ip;
	private ushort port;
	private uint code;
	private string redirect;
	private int resptype;
	private HTTP http;
	
	public this(string host, string ip, ushort port) {
		this.host = host;
		this.ip = ip;
		this.port = port;
		this.path = path;
		
	}
	
	public void parse(string headers, string path) {
		this.hdrs = headers;
		this.path = path;
		extractCodeHTTP();
		redirect = "o_O";
		this.classify();
	}

	public int getRespType() {
		return resptype;
	}
	
	public string getRedirection() {
		if (startsWith(redirect,"http")) {
			string[] parts = split(redirect,"/");
			return "/" ~ join(parts[3..$],"/") ~ "/";
		}
		
		return redirect;
	}
	
	private void extractCodeHTTP() {
		code = 0;
		if (this.hdrs.length <15)
			return;
		
		try {
			code = to!uint(this.hdrs[9..12]);
		} catch (Exception e) {
			code = 0;
		}
	}
	
	private string code2color(int code) {
		switch(code) { // TODO: integer optimization jmp table
			case 200:
				return paint.green;
			case 301:
			case 302:
			case 303:
				return paint.cyan;
			case 401:
				return paint.magenta;
			case 403:
				return paint.yellow;
			case 404:
				return "";
			default:
				return paint.blue;
		}
	}
	
	private void show(int code, string msg) {
		printf("%s%s",paint.clear.ptr,code2color(code).ptr);
		writef("[%d %s",code,msg);
		printf("%s\n",paint.clean.ptr);
	}
	
	private void extractLocation() {
		foreach(hdr; splitLines(this.hdrs)) {
			if (startsWith(hdr,"Location: ")) {
				this.redirect = split(hdr," ")[1];
			}
		}
	}
	
	public void classify() {
		
		switch(code) {
			case 200:
			
				auto http = new HTTP(host,ip,port);
				http.download(path);
				string html = ""; //http.read();
				
												
					if (endsWith(path,"/")) {
						show(code,"Ok DirListing] (" ~ to!string(html.length) ~ " bytes)\thttp://" ~ host ~ path);
						resptype = http_resp.FOUND_DIRINDEX;
					} else { 
						show(code,"Ok] (" ~ to!string(html.length) ~ " bytes)\thttp://" ~ host ~ path);
						resptype = http_resp.FOUND_OBJ;
					}
				//});
				//writeln("download finished");
		
				/*
				HTML page = new HTML(to!string(html), path);
				if (page.isDirlisting()) {
					show(code,"[200 Ok Dirlisting]\t"~path);
					return FOUND_DIRLIST;
					// crawl
					
				} else {
					show(code,"[200 Ok] (" ~ "TODO" ~ " bytes)\t" ~ url.getUrl());
					return FOUND_OBJ; // ¿¿can be a dir??
				}*/
				
				break;
	
			case 301:
			case 302:
			case 303:
				extractLocation();
				show(code,"Redirect]\t\thttp://" ~ host~path ~ " -> " ~ redirect);
				//show("[" ~ scode ~ "Redirect]\t\t" ~ path ~ " -> " ~ this.conn.responseHeaders()["location"]);
				resptype = http_resp.REDIRECT;
				
				/*
				auto loc = new URL(this.conn.responseHeaders()["location"]);
				if (loc.getHost() == url.getHost && 
					this.conn.responseHeaders()["location"] != url.getUrl() && 
					loc.getPort() == url.getPort()) 
						this.classify(loc.getPath(),get(loc.getPath(),this.conn)); // limitar recursividad? */
				break;
	
			case 401:
				show(code,"Auth needed]\thttp://"~host~path);
				resptype = http_resp.AUTH;
				return;
	
			case 403:
				show(code,"Directory found]\thttp://"~host~path);
				resptype = http_resp.FOUND_DIRNOLIST;
				break;
				
				/*
				auto resp2 = get(path~"/ajoeifjaweofiaj",this.conn);
	
				if (this.conn.statusLine().code == 404) {
					show(paint.yellow, "[403 Dirlisting denied]\t"~url.getUrl());
					allpush(url.getPath());
				} else 
					show(paint.yellow, "[403 Permission denied]\t"~url.getUrl());
				break;*/
	
			case 404:
				resptype = http_resp.NOT_FOUND;
				break;
				
			case 0:
				resptype = http_resp.FAIL;
				break;
				
			default:
				show(code,"]  \thttp://" ~ host~path);
				resptype = http_resp.OTHER;
				break;
		}
	}
}








