import std.string;
import std.stdio;
import std.regex;
import std.conv;
import std.algorithm;

class URL {

	private string url;
	private string base;
	private string proto;
	private bool ssl;
	private string hostport;
	private string host;
	private ushort port;
	private string path;
	private string dir;
	private string params;


	public this(string url) {
		this.url = url;
		this.parse(url);
	}



	public string fixUrl(string url) {
		url = replace(url, regex("\x0d"),"%0d");
		url = replace(url, regex(r"/\./"),"/");
		url = replace(url, regex(r"/\.\./"),"/");
		url = replace(url, regex("#"),"");

		if (!startsWith(url,"http"))
			url = "http://" ~ url;

		string[] http = cast(string[])std.string.split(cast(char[])url, "://");
		http[1] = replace(http[1], regex("//+"), "/");
		url = http[0] ~ "://" ~ http[1];

        return url;
	}




	public void parse(string url) {
		try {

			url = this.fixUrl(url);
			this.url = url;

			string[] parts = cast(string[])std.string.split(cast(char[])url,"/");
			this.proto = parts[0][0..$];
			this.ssl = (this.proto == "https");
			this.port = (this.ssl?443:80);
			this.hostport = parts[2];
			string[] hostparts =  cast(string[])std.string.split(this.hostport, ":");
			if (hostparts.length == 0) {
				this.host = "";
				return;
			}
			this.host = hostparts[0];
			if (hostparts.length > 1)
				this.port = to!ushort(hostparts[1]);

			this.base = this.proto ~ "//" ~ this.hostport;

			this.path = std.array.replace(this.url, this.base, "");
			if (this.path.length == 0)
				this.path = "/";
				
			this.params = "";
			parts = cast(string[])std.string.split(cast(char[])this.path, "?");
			if (parts.length > 0) {
				this.path = parts[0];

				if (parts.length > 1)
					this.params = parts[1];
			}

		} catch(Exception e) {
			writeln(e.msg);
		}

	}

	public bool isDirectory() {
		return endsWith(this.path, "/");
	}

	public string getDirectory() {
		return replace(url, regex("[^/]+$"), "/");
	}

	public string getUrl() {
		return this.url;
	}
	
	public string getBase() {
		return this.base;
	}
	
	public string getProto() {
		return this.proto;
	}
	
	public bool isSSL() { 
		return this.ssl;
	}
	
	public string getHostport() {
		return this.hostport;
	}
	
	public string getHost() {
		return this.host;
	}
	
	public ushort getPort() {
		return this.port;
	}
	
	public string getPath() {
		return this.path;
	}

	public string getParams() {
		return this.params;
	}

}


void test() {
	URL url = new URL("http://google.com");
	writeln(url.getPort());
	writeln(url.getBase());
	writeln(url.getPath());
	writeln(url.getHost());
}

