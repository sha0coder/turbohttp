DEBUG:=-g
DDEBUG:=-debug -g # -gc -gs -gx




DFLAGS:=-inline -release 
DC:=dmd 		# apt-get install dmd
LIB=socketpool/socketpool.c

all:
	$(DC) $(DFLAGS) src/* -of"dirscan"
	rm -f *.o
	
debug:
	$(DC) $(DDEBUG) src/* -of"dirscan"
	rm -f *.o
	
clean:
	rm -f *.o
	rm -f dirscan
