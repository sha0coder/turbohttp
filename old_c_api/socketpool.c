/*
 * bruetpoll very fast TCP bruteforcing library.
 *
 * Very fast bruteforcing library for protocols over TCP.
 * Easy to use, very fast, cpu friendly, asynchronous (read/write/end callbacks)
 *
 * by @sha0coder
 *
 *
 * TODO: provide an api function to reconnect a socket
 *
 */


#include <stdio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <string.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>

#include "socketpool.h"

int stop;


struct socketpool_callbacks cbs;


struct sockaddr_in getSockaddr(char *ip, unsigned short port) {
   static struct sockaddr_in sa;

   bzero(&sa, sizeof(sa));

   sa.sin_family = AF_INET;
   sa.sin_addr.s_addr = inet_addr(ip);
   sa.sin_port = htons(port);

   return sa;
}

int getSocket() {
	int sfd;
	int yes = 1;

	if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) <0) {
		perror("socket");
		abort();
	}

	if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) <0) {
		perror("setsockopt");
		abort();
	}

	if (fcntl(sfd, F_SETFL, O_NONBLOCK) == -1) {
		perror("fnctl");
		abort();
	}

	return sfd;
}

void doConnect(int sfd, struct sockaddr_in target) {
	if(connect(sfd, (struct sockaddr *)&target, sizeof(struct sockaddr)) == -1 && errno != EINPROGRESS) {
		if (errno == EAGAIN) {
			perror("connect is EAGAIN");
			close(sfd);
			abort();
		}
	}
}

int isSocketError(int sfd) {
   int ret;
   int code;
   size_t len = sizeof(int);

   ret = getsockopt(sfd, SOL_SOCKET, SO_ERROR, &code, &len);
   if ((ret || code)!= 0)
      return 1;

   return 0;
}

void onInterrupt(int signalId) {
	stop=signalId;
}

void setInterrupt() {
	struct sigaction sa;

	memset(&sa, 0, sizeof(struct sigaction *));
	sa.sa_handler = onInterrupt;
	sa.sa_flags = 0;
	sigemptyset (&(sa.sa_mask));
	if (sigaction (SIGINT, &sa, NULL)!= 0) {
		perror("sigaction");
		exit(1);
	}
}

void addEvent(int efd, int sfd) {
	static struct epoll_event ev;

	ev.events = EPOLLOUT | EPOLLIN | EPOLLRDHUP | EPOLLERR | EPOLLET ;
	ev.data.fd = sfd;

	if (epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &ev) != 0) {
		perror("epoll_ctl, adding event\n");
		abort();
	}
}

void delEvent(int efd, int sfd) {
	epoll_ctl(efd, EPOLL_CTL_DEL, sfd, NULL);
}

void eventLoop(int efd, int pool, struct sockaddr_in target) {
	static struct epoll_event *events;
	char *buff;
	int i,c;

	if ((buff = (char *)malloc(BUFFER+4)) == NULL) {
		perror("malloc buffer");
		abort();
	}

	if ((events = calloc(pool, sizeof(struct epoll_event))) == NULL) {
		perror("calloc events");
		abort();
	}

	do {
		c = epoll_wait(efd, events, pool, TIMEOUT);
		if (c == -1) {
			printf("epoll_wait problem\n");
			return;
		}

		for (i=0;i<c;i++) {

			if (events[i].events & (EPOLLRDHUP | EPOLLHUP))
				evDoReconnect(efd, &events[i], target);
			else {
				if (events[i].events & EPOLLIN)				// pirorize receiving before sending
					evDoRead(efd, &events[i], buff);

				if (events[i].events & EPOLLOUT)
					evDoWrite(efd, &events[i]);
			}

		}

	} while(!stop);

	printf("finalizando ...\n");
	cbs.finish(stop);
}

void evDoWrite(int efd, struct epoll_event *ev) {
	int bytes;
	int sz;
	char *ptr;

	if (isSocketError(ev->data.fd) != 0) {
		//perror("write isSocketError SITE DOWN!");
		stop = 1;
		cbs.sitedown();
		return;
	}

	ptr = cbs.write(ev->data.fd);
	if (ptr == NULL)
		return;

	//printf("=>%s<=\n",ptr);

	sz = strlen(ptr);
	if ((bytes = send(ev->data.fd, ptr, sz, 0)) < 0) {
		perror("send failed");
		return;
	}

	if (bytes < sz) {
		printf("sent %d bytes from a %d buffer\n",bytes,sz);
	}

	//printf("sending %d bytes!!\n",bytes);

}

void evDoRead(int efd, struct epoll_event *ev, char *buff) {
	int bytes;

	if(isSocketError(ev->data.fd) != 0) {
		//perror("read isSocketError SITE DOWN!");
		stop = 1;
		cbs.sitedown();
		return;
	}

    memset(buff,0x0,BUFFER+2);
    if ((bytes = recv(ev->data.fd, buff, BUFFER, 0)) < 0) {
		perror("recv failed");
		return;
	}

   	//printf("recv %d bytes!!\n",bytes);

    if (bytes>0 && cbs.read) // listo para leer y leo cero, ?? reconectar?
    	cbs.read(ev->data.fd,bytes,buff);
}

void evDoReconnect(int efd, struct epoll_event *ev, struct sockaddr_in target) {
	delEvent(efd, ev->data.fd);
	if (close(ev->data.fd)!=0) { //todo shutdown
		perror("close");
		return;
	}

	//printf("reconnecting\n");
	newConnection(efd, target);

	if (ev->events & EPOLLERR) {
		perror("epoll");
		return;
	}
}

void newConnection(int efd, struct sockaddr_in target) {
	int sfd = getSocket();
	doConnect(sfd, target);
	addEvent(efd, sfd);
}





// API

extern void stopSocketpool() {
	stop = 1;
}

extern void socketPool(char *ip, unsigned short port, int pool) {
	struct sockaddr_in target;
	int efd,sfd,i;

	setInterrupt();
	target = getSockaddr(ip,port);
	efd = epoll_create(pool);

	for (i=0; i<pool; i++)
		newConnection(efd, target);

	eventLoop(efd, pool, target);
}

extern void setPoolRead(void (*cbRead)(int sfd, size_t sz, char *buff)) {
	cbs.read = cbRead;
}

extern void setPoolWrite(char *(*cbWrite)(int sfd)) {
	cbs.write = cbWrite;
}

extern void setPoolFinish(void (*cbFinish)(int sig)) {
	cbs.finish = cbFinish;
}

extern void setSiteDown(void (*cbSiteDown)()) {
	cbs.sitedown = cbSiteDown;
}







