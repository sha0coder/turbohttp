module socketpool;

import std.stdio;

extern (C) void socketPool(char *ip, ushort port, int connections);
extern (C) void stopSocketpool();
extern (C) void setPoolRead(void function(int sfd, size_t sz, char *buff) cbRead);
extern (C) void setPoolWrite(char *function(int sfd) cbWrite);
extern (C) void setPoolFinish(void function(int sig) cbFinish);
extern (C) void setSiteDown(void function() cbSiteDown);


class SocketPool {
	private string ip;
	private ushort port;
	private int pool;
	
	public this(string ip, ushort port) {
		this.ip = ip;
		this.port = port;
		this.pool = 10;
	}
	
	public void setPool(int pool) {
		this.pool = pool;
	}
	
	public void start() {
		try {
			socketPool(cast(char*)ip,port,pool);
		} catch(Exception e) {
			writeln("socketPool failed o_O");
		}
	}
	
	public void stop() {
		stopSocketpool();
	}
	
	// INSTALL EVENTS
	
	extern (C) public void onRead(void function(int sfd, size_t sz, char *buff) cbRead) {
		setPoolRead(cbRead);
	}
	
	extern (C) public void onWrite(char *function(int sfd) cbWrite) {
		setPoolWrite(cbWrite);
	}
	
	extern (C) public void onFinish(void function(int sig) cbFinish) {
		setPoolFinish(cbFinish);
	}
	
	extern (C) public void onSiteDown(void function() cbSiteDown) {
		setSiteDown(cbSiteDown);
	}
	
}