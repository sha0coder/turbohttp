// http://en.wikibooks.org/wiki/Python_Programming/Extending_with_C

#include <Python.h>
#include "socketpool.h"

// python callbacks
static PyObject *cbRead = NULL;
static PyObject *cbWrite = NULL;
static PyObject *cbFinish = NULL;
static PyObject *cbOnServerDown = NULL;

// c callbacks wraper to call python callbacks
void wrap_cbRead(int sfd, size_t sz, char *buff) {
	PyObject *arglist;
	PyObject *result;

	arglist = Py_BuildValue("(i)(l)(s)", sfd, sz, buff);
	result = PyEval_CallObject(my_callback, arglist);
	Py_DECREF(arglist);
}

char *warp_cbWrite(int sfd) {
}

void wrap_cbFinish(int sig) {
}

void wrap_cbSiteDown() {
}




// python callback setters
static PyObject *socketpool_onRead(PyObject *self, PyObject *args) {
	PyObject *cb;

	if (!PyArg_ParseTuple(args, "O:set_callback", &cb)
        	return NULL;

	if (!PyCallable_Check(cb)) 
		return NULL;

	Py_XINCREF(cb);
	Py_XDECREF(cbRead);
	cbRead = cb;
	
	Py_INCREF(Py_None);
        return Py_None;	
}


static PyObject *socketpool_onWrite(PyObject *self, PyObject *args) {
	PyObject *cb;

	if (!PyArg_ParseTuple(args, "O:set_callback", &cb)
        	return NULL;

	if (!PyCallable_Check(cb)) 
		return NULL;

	Py_XINCREF(cb);
	Py_XDECREF(cbWrite);
	cbRead = cb;
	
	Py_INCREF(Py_None);
        return Py_None;	
}


static PyObject *socketpool_onFinish(PyObject *self, PyObject *args) {
	PyObject *cb;

	if (!PyArg_ParseTuple(args, "O:set_callback", &cb)
        	return NULL;

	if (!PyCallable_Check(cb)) 
		return NULL;

	Py_XINCREF(cb);
	Py_XDECREF(cbFinish);
	cbRead = cb;
	
	Py_INCREF(Py_None);
        return Py_None;	
}


static PyObject *socketpool_onServerDown(PyObject *self, PyObject *args) {
	PyObject *cb;

	if (!PyArg_ParseTuple(args, "O:set_callback", &cb)
        	return NULL;

	if (!PyCallable_Check(cb)) 
		return NULL;

	Py_XINCREF(cb);
	Py_XDECREF(cbServerDown);
	cbRead = cb;

	Py_INCREF(Py_None);
        return Py_None;	
}



// python methods
static PyObject *socketpool_start(PyObject *self, PyObject *args) {
	char *ip;
	unsigned short port;
	int pool;
	
	if (!PyArg_ParseTuple(args, "sui", &ip,&port,&pool))
        	return NULL;

	socketPool(ip,port,pool);

        Py_INCREF(Py_None);
        return Py_None;
}


static PyObject *socketpool_stop(PyObject *self, PyObject *args) {
	stopSocketpool();

        Py_INCREF(Py_None);
        return Py_None;
}

static PyMethodDef socketpoolMethods[] = {
	{"onRead", socketpool_onRead, METH_VARARGS, "read callback, socket pool ready for read"},
	{"onWrite", socketpool_onRead, METH_VARARGS, "write callback, socket pool ready for write"},
	{"onFinish", socketpool_onRead, METH_VARARGS, "pool finished"},
	{"onServerDown", socketpool_onRead, METH_VARARGS, "fired if the target is down"},
	{"start", socketpool_start, METH_VARARGS, "start the socket pool"},
	{"stop", socketpool_stop, METH_VARARGS, "stop the socket pool"},
	{NULL,NULL,0,NULL}
};


PyMODINIT_FUNC

initsocketpool(void) {
	(void) Py_InitModule("SocketPool", socketpoolsMethods); //constructor

	setPoolRead(wrap_cbRead);
	setPoolWrite(wrap_cbWrite);
	setPoolFinish(wrap_cbFinish);
	setSiteDown(wrap_cbSiteDown);
}

int main (int argc, char **argv) {
	Py_SetProgramName(argv[0]);
	Py_Initialize();
	initsocketpool();
}

