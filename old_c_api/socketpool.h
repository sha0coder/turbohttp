#define TIMEOUT 1000
#define BUFFER 1024*5



struct socketpool_callbacks {
	void (*read)(int sfd, size_t sz, char *buff);
	char *(*write)(int sfd);
	void (*sitedown)(void);
	void (*finish)(int sig);
};




// SocketPool API //

extern void stopSocketpool();
extern void socketPool(char *ip, unsigned short port, int pool);

extern void setPoolRead(void (*cbRead)(int sfd, size_t sz, char *buff));
extern void setPoolWrite(char *(*cbWrite)(int sfd));
extern void setPoolFinish(void (*cbFinish)(int sig));
extern void setSiteDown(void (*cbSiteDown)());



